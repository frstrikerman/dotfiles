local gears = require("gears")
local lain  = require("lain")
local awful = require("awful")
local wibox = require("wibox")
local os = { getenv = os.getenv }
local my_table = awful.util.table or gears.table -- 4.{0,1} compatibility
local pywal = require ("themes.wal_nerdfonts.pywal")
local nerdfonts = require ("themes.wal_nerdfonts.nerd-fonts")
local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi
local theme                                     = {}
local gfs = require("gears.filesystem")
local themes_path = gfs.get_themes_dir()
theme.dir                                       = os.getenv("HOME") .. "/.config/awesome/themes/wal_nerdfonts/"
theme.wallpaper                                 = pywal.wallpaper
theme.font                                      = "Hack Nerd Font 10"
theme.fg_normal                                 = pywal.special.foreground
theme.fg_focus                                  = pywal.colors.color0
theme.fg_urgent                                 = pywal.colors.color0
theme.bg_normal                                 = pywal.colors.color0
theme.bg_focus                                  = pywal.colors.color13
theme.bg_urgent                                 = pywal.colors.color8
theme.border_width                              = dpi(1)
theme.border_normal                             = pywal.colors.color0
theme.border_focus                              = pywal.colors.color13
theme.border_marked                             = pywal.colors.color8
theme.tasklist_bg_focus                         = theme.bg_focus
theme.titlebar_bg_focus                         = theme.bg_focus
theme.titlebar_bg_normal                        = theme.bg_normal
theme.titlebar_fg_focus                         = theme.fg_focus
theme.titlebar_close_button = "true"
theme.menu_height                               = dpi(15)
theme.menu_width                                = dpi(100)
theme.tasklist_plain_task_name                  = false
theme.tasklist_disable_icon                     = false
theme.useless_gap                               = 5
-- Define the image to load
theme.titlebar_close_button_focus               = theme.dir .. "/icons/titlebar/close_focus.png"
theme.titlebar_close_button_normal              = theme.dir .. "/icons/titlebar/close_normal.png"
theme.titlebar_ontop_button_focus_active        = theme.dir .. "/icons/titlebar/ontop_focus_active.png"
theme.titlebar_ontop_button_normal_active       = theme.dir .. "/icons/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_inactive      = theme.dir .. "/icons/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_inactive     = theme.dir .. "/icons/titlebar/ontop_normal_inactive.png"
theme.titlebar_sticky_button_focus_active       = theme.dir .. "/icons/titlebar/sticky_focus_active.png"
theme.titlebar_sticky_button_normal_active      = theme.dir .. "/icons/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_inactive     = theme.dir .. "/icons/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_inactive    = theme.dir .. "/icons/titlebar/sticky_normal_inactive.png"
theme.titlebar_floating_button_focus_active     = theme.dir .. "/icons/titlebar/floating_focus_active.png"
theme.titlebar_floating_button_normal_active    = theme.dir .. "/icons/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_inactive   = theme.dir .. "/icons/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_inactive  = theme.dir .. "/icons/titlebar/floating_normal_inactive.png"
theme.titlebar_maximized_button_focus_active    = theme.dir .. "/icons/titlebar/maximized_focus_active.png"
theme.titlebar_maximized_button_normal_active   = theme.dir .. "/icons/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_inactive  = theme.dir .. "/icons/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_inactive = theme.dir .. "/icons/titlebar/maximized_normal_inactive.png"

-- You can use your own layout icons like this:
theme.layout_tile                               = theme.dir .. "/icons/layouts/tile.png"
theme.layout_tileleft                           = theme.dir .. "/icons/layouts/tileleft.png"
theme.layout_tilebottom                         = theme.dir .. "/icons/layouts/tilebottom.png"
theme.layout_tiletop                            = theme.dir .. "/icons/layouts/tiletop.png"
theme.layout_fairv                              = theme.dir .. "/icons/layouts/fairv.png"
theme.layout_fairh                              = theme.dir .. "/icons/layouts/fairh.png"
theme.layout_spiral                             = theme.dir .. "/icons/layouts/spiral.png"
theme.layout_dwindle                            = theme.dir .. "/icons/layouts/dwindle.png"
theme.layout_max                                = theme.dir .. "/icons/layouts/max.png"
theme.layout_fullscreen                         = theme.dir .. "/icons/layouts/fullscreen.png"
theme.layout_magnifier                          = theme.dir .. "/icons/layouts/magnifier.png"
theme.layout_floating = theme.dir .. "/icons/layouts/floating.png"
-- You can use your own layout icons like this:
-- Generate Awesome icon:
theme.awesome_icon = theme_assets.awesome_icon(
  theme.menu_height, theme.bg_focus, theme.fg_focus
)

local taglist_square_size = dpi(4)
theme.taglist_squares_sel = theme_assets.taglist_squares_sel(
  taglist_square_size, theme.fg_normal
)
theme.taglist_squares_unsel = theme_assets.taglist_squares_unsel(
  taglist_square_size, theme.fg_normal
)

local markup = lain.util.markup
local separators = lain.util.separators

-- Textclock
local clockicon = wibox.widget.imagebox(theme.widget_clock)
local clock = awful.widget.watch(
  "date +'%a %d %b %R'", 60,
  function(widget, stdout)
    widget:set_markup(" " .. markup.font(theme.font, stdout))
  end
)

-- Calendar
calendaricon = wibox.widget.textbox(nerdfonts.font_awesome.calendar)
calendaricon.font = theme.font
theme.cal = lain.widget.calendar({
    attach_to = { clock },
    notification_preset = {
      font = theme.font,
      fg   = theme.fg_normal,
      bg   = theme.bg_normal
    }
})

-- Mail IMAP check
local mailicon = wibox.widget.textbox(nerdfonts.font_awesome.envelope)
mailicon.font = theme.font
--[[ commented because it needs to be set before use
  mailicon:buttons(my_table.join(awful.button({ }, 1, function () awful.spawn(mail) end)))
  local mail = lain.widget.imap({
  timeout  = 180,
  server   = "server",
  mail     = "mail",
  password = "$(pass show passwd/Windows/",
  settings = function()
  if mailcount > 0 then
  widget:set_text(" " .. mailcount .. " ")
  mailicon.text = nerdfonts.font_awesome.envelope
  else
  widget:set_text("") 
  mailicon.markup = markup.font(theme.font, markup(pywal.colors.color1, nerdfonts.font_awesome.envelopg) .. title)
  end
  end
  })
--]]

-- MPD
local musicplr = awful.util.terminal .. " -title Music -g 130x34-320+16 -e ncmpcpp"
mpdicon = wibox.widget.textbox("")
mpdicon.font = theme.font
mpdicon:buttons(my_table.join(
                  awful.button({ modkey }, 1, function () awful.spawn.with_shell(musicplr) end),
                  awful.button({ }, 1, function ()
                      awful.spawn.with_shell("mpc prev")
                      theme.mpd.update()
                  end),
                  awful.button({ }, 2, function ()
                      awful.spawn.with_shell("mpc toggle")
                      theme.mpd.update()
                  end),
                  awful.button({ }, 3, function ()
                      awful.spawn.with_shell("mpc next")
                      theme.mpd.update()
end)))
theme.mpd = lain.widget.mpd({
    host="192.169.1.29",
    port="6600",
    settings = function()
      if mpd_now.state == "play" then
        artist = " " .. mpd_now.artist .. " "
        title  = mpd_now.title  .. " "
        mpdicon.text = nerdfonts.font_awesome.play
      elseif mpd_now.state == "pause" then
        artist = " mpd "
        title  = "paused "
        mpdicon.text = nerdfonts.font_awesome.pause
      else
        artist = ""
        title  = ""
        mpdicon.text = nerdfonts.material_design.music_note
      end
      mpdicon.text = mpdicon.text .. " "	mpdicon.text = mpdicon.text .. " "
      widget:set_markup(markup.font(theme.font, markup(pywal.colors.color5, artist) .. title))
    end
})

-- MEM
local memicon = wibox.widget.textbox(nerdfonts.material_design.chip)
memicon.font = theme.font
local mem = lain.widget.mem({
    settings = function()
      widget:set_markup(markup.font(theme.font, " " .. mem_now.used .. "MB "))
    end
})

-- CPU
local cpuicon = wibox.widget.textbox(nerdfonts.material_design.poll)
cpuicon.font = theme.font
local cpu = lain.widget.cpu({
    settings = function()
      widget:set_markup(markup.font(theme.font, " " .. cpu_now.usage .. "% "))
    end
})

-- Coretemp
local tempicon = wibox.widget.textbox(nerdfonts.font_awesome.thermometer_half)
tempicon.font = theme.font
local temp = lain.widget.temp({
    settings = function()
      widget:set_markup(markup.font(theme.font, " " .. coretemp_now .. "°C "))
    end
})

-- / fs
local fsicon = wibox.widget.textbox(nerdfonts.material_design.harddisk)
fsicon.font = theme.font
theme.fs = lain.widget.fs({
    notification_preset = { fg = theme.fg_normal, bg = theme.bg_normal, font = theme.font },
    settings = function()
      widget:set_markup(markup.font(theme.font, " " .. fs_now["/home"].percentage .. "% "))
    end
})

-- Battery
local baticon = wibox.widget.textbox(nerdfonts.font_awesome.battery)
baticon.font = theme.font
local bat = lain.widget.bat({
    battery = "BAT0",
    settings = function()
      if bat_now.status ~= "N/A" then
        if bat_now.ac_status == 1 then
          widget:set_markup(markup.font(theme.font, " AC "))
          baticon.text = nerdfonts.material_design.plug
          return
        elseif bat_now.perc and tonumber(bat_now.perc) <= 5 then
          baticon.text = nerdfonts.font_awesome.battery_empty
        elseif bat_now.perc and tonumber(bat_now.perc) <= 15 then
          baticon.text = nerdfonts.font_awesome.battery_quarter
        elseif bat_now.perc and tonumber(bat_now.perc) <= 50 then
          baticon.text = nerdfonts.font_awesome.battery_half
        elseif bat_now.perc and tonumber(bat_now.perc) <= 75 then
          baticon.text = nerdfonts.font_awesome.battery_three_quarters
        else
          baticon.text = nerdfonts.font_awesome.battery_full
        end
        widget:set_markup(markup.font(theme.font, " " .. bat_now.perc .. "% "))
      end
    end
})

local bat1icon = wibox.widget.textbox(nerdfonts.font_awesome.battery)
bat1icon.font = theme.font
local bat1 = lain.widget.bat({
    battery = "BAT1",
    settings = function()
      if bat_now.status ~= "N/A" then
        if bat_now.ac_status == 1 then
          widget:set_markup(markup.font(theme.font, " AC "))
          bat1icon.text = nerdfonts.material_design.plug
          return
        elseif bat_now.perc and tonumber(bat_now.perc) <= 5 then
          bat1icon.text = nerdfonts.font_awesome.battery_empty 
        elseif bat_now.perc and tonumber(bat_now.perc) <= 15 then
          bat1icon.text = nerdfonts.font_awesome.battery_quarter
        elseif bat_now.perc and tonumber(bat_now.perc) <= 50 then
          bat1icon.text = nerdfonts.font_awesome.battery_half
        elseif bat_now.perc and tonumber(bat_now.perc) <= 75 then
          bat1icon.text = nerdfonts.font_awesome.battery_three_quarters
        else
          bat1icon.text = nerdfonts.font_awesome.battery_full
        end
        widget:set_markup(markup.font(theme.font, " " .. bat_now.perc .. "% "))
      end
    end
})
local volicon = wibox.widget.textbox(nerdfonts.octicons.mute .. " ")
volicon.font = theme.font
theme.volume = lain.widget.alsa({
    channel = "Master",
    settings = function()
      if volume_now.status == "off" then
        volicon.text = nerdfonts.octicons.mute .. " "
      elseif tonumber(volume_now.level) == 0 then
        volicon.text = nerdfonts.font_awesome.volume_off .. " "
      elseif tonumber(volume_now.level) <= 50 then
        volicon.text = nerdfonts.font_awesome.volume_down .. " "
      else
        volicon.text = nerdfonts.font_awesome.volume_up .. " "
      end

      widget:set_markup(markup.font(theme.font, volume_now.level .. "% "))
    end
})

-- Net
local neticon = wibox.widget.textbox(nerdfonts.material_design.ethernet)
neticon.font = theme.font
local net = lain.widget.net({
    settings = function()
      widget:set_markup(markup.font(theme.font,
                                    markup(pywal.colors.color5, " " .. net_now.received)
                                      .. 
                                      markup(pywal.colors.color14, " " .. net_now.sent .. " ")))
    end
})

local mykeyboardlayout = awful.widget.keyboardlayout()
local keyboardIcon = wibox.widget.textbox(nerdfonts.material_design.keyboard_variant)
keyboardIcon.font = theme.font
-- Separators
local spr     = wibox.widget.textbox(' ')

function theme.at_screen_connect(s)
  -- Quake application
  s.quake = lain.util.quake({ app = awful.util.terminal })

  -- If wallpaper is a function, call it with the screen
  local wallpaper = theme.wallpaper
  if type(wallpaper) == "function" then
    wallpaper = wallpaper(s)
  end
  gears.wallpaper.maximized(wallpaper, s, true)

  -- Tags
  awful.tag(awful.util.tagnames, s, awful.layout.layouts)

  -- Create a promptbox for each screen
  s.mypromptbox = awful.widget.prompt()
  -- Create an imagebox widget which will contains an icon indicating which layout we're using.
  -- We need one layoutbox per screen.
  s.mylayoutbox = awful.widget.layoutbox(s)
  s.mylayoutbox:buttons(my_table.join(
                          awful.button({ }, 1, function () awful.layout.inc( 1) end),
                          awful.button({ }, 3, function () awful.layout.inc(-1) end),
                          awful.button({ }, 4, function () awful.layout.inc( 1) end),
                          awful.button({ }, 5, function () awful.layout.inc(-1) end)))
  -- Create a taglist widget
  s.mytaglist = awful.widget.taglist(s, awful.widget.taglist.filter.all, awful.util.taglist_buttons)

  -- Create a tasklist widget
  s.mytasklist = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, awful.util.tasklist_buttons)

  -- Create the wibox
  s.mywibox = awful.wibar({ position = "top", screen = s, height = dpi(20), bg = theme.bg_normal, fg = theme.fg_normal })

  -- Add widgets to the wibox
  s.mywibox:setup {
    layout = wibox.layout.align.horizontal,
    { -- Left widgets
      layout = wibox.layout.fixed.horizontal,
      --spr,
      s.mytaglist,
      s.mypromptbox,
      spr,
    },
    s.mytasklist, -- Middle widget
    { -- Right widgets
      layout = wibox.layout.fixed.horizontal,
      wibox.widget.systray(),
      spr,
      keyboardIcon,
      mykeyboardlayout,
      spr,
      mpdicon,
      theme.mpd.widget,
      volicon,
      theme.volume.widget,
      --mailicon,
      --mail.widget,
      memicon,
      mem.widget,
      cpuicon,
      cpu.widget,
      tempicon,
      temp.widget,
      fsicon,
      theme.fs.widget,
      baticon,
      bat.widget,
      bat1icon,
      bat1.widget,
      neticon,
      net.widget,
      calendaricon,
      clock,
      spr,
      s.mylayoutbox,
    },
  }
end

return theme
